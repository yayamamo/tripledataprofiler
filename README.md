# Triple Data Profiler #

You can obtain several statistics on datasets through a given SPARQL endpoint.

## Requirements ##
* [Apache Jena](http://jena.apache.org/)
* [Java](https://java.com/)

## Compile (Example) ##
`$ javac -cp apache-jena-5.2.0/lib/*:./src ./src/jp/ac/rois/dbcls/TripleDataProfiler.java`

## Execution ##
```
$ java -jar TripleDataProfiler.jar  [-k <APIkey>|-w <wait time>|-timeout <timeout period>|-sbm|-s] -ep <Endpoint URL> [-gn <Named Graph Name(s)>] [-agn <Named Graph Name(s)>]
-----
   -k <APIkey>:               an API Key for an Endpoint that requires it.
   -w <wait time>:            a wait time between queries in milli seconds. Default time is 333 mSecs.
   -timeout <timeout period>: a timeout period for a query in seconds. Default is 900 Secs (=20 Mins).
   -sbm: a flag to output in the SBM format.
   -s:   a flag to print nothing during crawling
   -gn:  a named graph to be counted
   -agn: a named graph to be excluded from counting
```
OR

`$ java -cp apache-jena-5.2.0/lib/*:./src jp/ac/rois/dbcls/TripleDataProfiler [-k <APIkey>|-w <wait time>|-timeout <timeout period>|-sbm|-s] <Endpoint URL> [-gn <Named Graph Name(s)>] [-agn <Named Graph Name(s)>]` 

sbm: Output in the Turtle format compatible for the SPARQL builder metadata schema (SBM).

## License ##
MIT

---
Yasunori Yamamoto (yayamamo)

[Database Center for Life Science](http://dbcls.rois.ac.jp/)
