package jp.ac.rois.dbcls;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.jena.atlas.io.IndentedWriter;
import org.apache.jena.atlas.web.HttpException;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Node;
import org.apache.jena.query.ARQ;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.exec.http.QueryExecutionHTTPBuilder;
import org.apache.jena.sparql.exec.http.QuerySendMode;
import org.apache.jena.sparql.exec.http.UpdateExecutionHTTPBuilder;
import org.apache.jena.sparql.modify.request.UpdateWriter;
import org.apache.jena.sparql.serializer.SerializationContext;
import org.apache.jena.sys.JenaSystem;
import org.apache.jena.update.UpdateRequest;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.http.sys.HttpRequestModifier;
import org.apache.jena.http.sys.RegistryRequestModifier;

/**
 * @author yayamamo (Yasunori Yamamoto, Database Center for Life Science)
 * Issues several SPARQL queries to obtain the snapshot of the data structure (or schema) of a given SPARQL endpoint.
 * License: MIT
 */

public class TripleDataProfiler {
	static String shortHelpMessage = "Needs arguments: [-ep <Endpoint URL>|-gn <Named Graph Name(s)>|-agn <Named Graph Name(s)>|-k <APIkey>|-w <wait time>|-timeout <timeout period>|-sbm|-get|-s]\n"
			+ "-----\n"
			+ "   -ep <URL>: a target SPARQL endpoint URL.\n"
			+ "   -gn <Named Graph Name(s): graph name(s) to be considered\n"
			+ "   -agn <Named Graph Name(s): graph name(s) to be ignored\n"
			+ "   -k <APIkey>: an API Key for an Endpoint that requires it.\n"
			+ "   -w <wait time>: a wait time between queries in milli seconds. Default time is 333 mSecs.\n"
			+ "   -timeout <timeout period>: a timeout period for a query in seconds. Default is 900 Secs (=15 Mins).\n"
			+ "   -sbm: a flag to output in the SBM format.\n"
			+ "   -get: use the HTTP GET method to access the SPARQL endpoint. Default is false (i.e., use the HTTP POST method).\n"
			+ "   -s: a flag to print nothing during crawling.\n"
			+ "   -proxy_host <proxy host name>.\n"
			+ "   -proxy_port <proxy port number>.\n"
			+ "   -custom_endpoint <customized endpoint url> : SPARQL endpoint URL to be included for the sd:endpoint property.\n"
			+ "\n"
			+ " This Tool uses Apache Jena libraries (https://jena.apache.org/).";
	static String queryGraphs = "SELECT ?g WHERE { GRAPH ?g {?s ?p ?o} } GROUP BY ?g";
//	static String altQueryGraphs = "SELECT * WHERE { GRAPH ?g {} }";
	static String altQueryGraphs = "SELECT DISTINCT ?g WHERE { GRAPH ?g {?s ?p ?o} }";
	static String endpointUrl;
	static String customEpUrl;
	static String repoEndpointUrl = "http://localhost:8081/openrdf-sesame/repositories/SBM/statements";
	static Set<String> avoidableGraph = new HashSet<String>(Arrays.asList(
			"http://www.openlinksw.com/schemas/virtrdf#",
			"http://www.openlinksw.com/schemas/oplweb#",
			"http://localhost:8890/DAV",
			"http://localhost:8890/sparql",
			"http://www.w3.org/2002/07/owl",
			"http://www.w3.org/2002/07/owl#",
			"http://www.w3.org/2000/01/rdf-schema",
			"http://www.w3.org/1999/02/22-rdf-syntax-ns",
			"http://www.w3.org/ns/sparql-service-description",
			"http://www.w3.org/ns/r2rml#OVL",
			"http://www.w3.org/ns/ldp#",
			"b3sifp",
			"b3sonto",
			"virtrdf-label",
			"urn:virtuoso:val:acl:schema",
			"urn:rules.skos",
			"facets"
			));
	static boolean graphAcceptable = false;
	static int waittime = 500; // milliseconds
	static int shortTimeOut = 30; // seconds
	static int longTimeOut = 1200; // seconds = 20 mins
	static int maxTryCount = 3;
	static String userAgent = "TripleDataProfiler/1.1 from DBCLS [https://bitbucket.org/yayamamo/tripledataprofiler]";
	static long nOfQueries;
	static String apikey;
	static String proxyHost;
	static String proxyPort;
	static boolean enableConstruct;
	static boolean sbmFlag;
	static boolean insertFlag;
	static boolean silentFlag;
	static boolean countUnknownClass;
	static boolean getFlag;
	static Date start;
	static Date end;
	static long totalTriples;
	static String[] givenAvoidGraphs;
	static String[] givenGraphs;

	static Set<RDFNode> Classes;
	static Set<RDFNode> Properties;
	static HashMap<String,String> classLabels;
	static HashMap<RDFNode,Long> predData;
	static HashMap<RDFNode,HashSet<RDFNode>> predClassData;
	static HashMap<RDFNode,List<RDFNode>> predDomain;  
	static HashMap<RDFNode,List<RDFNode>> predRange;
	static HashMap<List<RDFNode>,Long> predRelCount;
	static HashMap<List<RDFNode>,String> predSample;
	static HashMap<RDFNode,Long> classData;

	static Resource unk = ModelFactory.createDefaultModel().createResource("[unknown]");
	static Resource rsc = ModelFactory.createDefaultModel().createResource(RDFS.Resource.getURI());
	static Resource lit = ModelFactory.createDefaultModel().createResource(RDFS.Literal.getURI());
	// static Resource lit = ModelFactory.createDefaultModel().createResource(XSDDatatype.XSDstring.getURI());

	static void insertData(Model m){
		Node repoGraph = ModelFactory.createDefaultModel().createResource("http://www.sparqlbuilder.org/").asNode();
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		UpdateWriter uw = new UpdateWriter(new IndentedWriter(bao), new SerializationContext(true));
		uw.open();
		StmtIterator st = m.listStatements();
		while(st.hasNext()){
			Quad qd = new Quad(repoGraph, st.next().asTriple());
			uw.insert(qd);
		}
		uw.close();
		//System.out.println(bao.toString());
		UpdateRequest ur = new UpdateRequest();
		ur.add(bao.toString());
		UpdateExecutionHTTPBuilder upr = UpdateExecutionHTTPBuilder.create();
		upr.endpoint(repoEndpointUrl);
		upr.update(ur);
		//UpdateProcessRemoteForm upr = new UpdateProcessRemoteForm(ur, repoEndpointUrl, null);
		try {
			upr.execute();
		} catch (Exception e){
			System.out.println(
					"#Error in insertData: " + e.getMessage() + "\t" +
							"Cause: " + (e.getCause() == null?e.getCause():"") + "\t" +
							"Exception: " + e.toString());
		}
	}

	static Object prepareQuery(String ep, String q, String queryType){
		HttpRequestModifier modifier = (params, headers)->{
			headers.put("User-Agent", userAgent);
		};
		RegistryRequestModifier.get().add(ep, modifier);
		QueryExecution gqexec = null;
		try {
			HttpClient httpClient = HttpClient.newBuilder()
	                .connectTimeout(Duration.ofSeconds(shortTimeOut))  // Timeout to connect
	                .followRedirects(Redirect.NORMAL)
	                .build();
			if(!getFlag) {
				gqexec = QueryExecutionHTTPBuilder
						.service(ep)
						.httpClient(httpClient)
						.query(q)
						.sendMode(QuerySendMode.asPost)
						.timeout(longTimeOut, TimeUnit.SECONDS) // Timeout of request
						.build();
			} else {
				gqexec = QueryExecutionHTTPBuilder
	                    .service(ep)
	                    .httpClient(httpClient)
	                    .query(q)
	                    .sendMode(QuerySendMode.asGetAlways)
	                    .timeout(longTimeOut, TimeUnit.SECONDS) // Timeout of request
	                    .build();
			}
			nOfQueries++;
			Map<String, Map<String,List<String>>> serviceParams = new HashMap<String, Map<String,List<String>>>();
			Map<String,List<String>> params = new HashMap<String,List<String>>();
			if(apikey != null){
				List<String> values = new ArrayList<String>();
				values.add(apikey);
				params.put("apikey", values);
				serviceParams.put(ep, params);
				gqexec.getContext().set(ARQ.serviceParams, serviceParams);
			}
		} catch (Exception e){
			System.out.println("#Error in prepareQuery\tSomething wrong with the query: " + q);
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		try {
			if(queryType == "SELECT") {
				ResultSet rs = gqexec.execSelect();
				return (ResultSet) rs;
			} else if(queryType == "CONSTRUCT") {
				Model m = gqexec.execConstruct();
				return (Model) m;
			} else {
				return null;
			}
		} catch (Exception e) {
			System.out.println("Something weird happened: " + e.getMessage());
			System.out.println("-----:(:(-:):)-----");
			return null;
		} finally {gqexec.close();}
	}

	static void waitAbit(){
		try {
			TimeUnit.MILLISECONDS.sleep(waittime);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	// Counts the triples whose subject and object belong to class1 and class2, respectively
	static int getClassRelations(String query, String class1, String class2, String erm) {
		int result = -1;
		for(int try_count = maxTryCount; try_count > 0; try_count--) {
			for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
			Date sd = new Date();
			System.err.println("S:" + sd);
			ResultSet crs = (ResultSet) prepareQuery(endpointUrl, query, "SELECT");
			if(crs == null) return 0;
			try {
				long x = crs.next().get("rc").asLiteral().getLong();
				if(!(sbmFlag || silentFlag))
					System.out.println("Crel\t" + x + "\t" + class1 + " -> " + class2);
				result = (x > 0)? 1:0;
				if(try_count < maxTryCount)
					System.out.println("#Success after error.");
				try_count = 0;
			} catch (Exception e) {
				Date ed = new Date();
				System.err.println("E:" + ed);
				System.out.println("#Error in getClassRelations [" + try_count + ":" + sd + " - " + ed + "]: " + erm + " -- " + e.getMessage() + " -- " + query);
			} finally { crs.close(); }
		}
		return result;
	}

	// Enumerates the predicates given by query1 and shows triples given by query2 w.r.t each predicate
	static void getPredicates(String query1, String query2, String erm, RDFNode c1, RDFNode c2){
		for(int try_count = maxTryCount; try_count > 0; try_count--) {
			for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
			ResultSet rs = (ResultSet) prepareQuery(endpointUrl, query1, "SELECT");
			if(rs == null) return;
			try {
				for ( ; rs.hasNext() ;){
					QuerySolution rsn = rs.nextSolution();
					long x = rsn.get("rc").asLiteral().getLong();
					RDFNode rn = rsn.get("p");
					Properties.add(rn);
					String pred = rn.toString();
					String predLabel = classLabels.containsKey(pred)? "\t" + classLabels.get(pred):"";
					if(predData.containsKey(rn)){
						predData.put(rn, predData.get(rn) + x);
					}else{
						predData.put(rn, x);
						predClassData.put(rn, new HashSet<RDFNode>());
						predDomain.put(rn, new ArrayList<RDFNode>());
						predRange.put(rn, new ArrayList<RDFNode>());
					}
					if(c1 != null)
						predClassData.get(rn).add(c1);
					if(c2 != null)
						predClassData.get(rn).add(c2);
					RDFNode dom = (c1 == null)?unk:c1;
					RDFNode ran = (c2 == null)?unk:c2;
					predDomain.get(rn).add(dom);
					predRange.get(rn).add(ran);
					List<RDFNode> trp = Arrays.asList(rn,dom,ran);
					if(predRelCount.containsKey(trp))
						predRelCount.put(trp, predRelCount.get(trp) + x);
					else
						predRelCount.put(trp, x);
					if(!(sbmFlag || silentFlag))
						System.out.println("Relp\t" + x + "\t" + pred + predLabel);
					String rq = query2.replace("__PRED__", pred);
					Model qm = (Model) prepareQuery(endpointUrl, rq, "CONSTRUCT");
					if(qm == null) return;
					waitAbit();
					if(enableConstruct){
						try {
							StringWriter w = new StringWriter();
							qm.write(w, "TTL");
							String str = w.toString();
							predSample.put(trp, str);
							if(!(sbmFlag || silentFlag)){
								str = str.replaceAll("\\r\\n\\r\\n", "\r\n").replaceAll("\\r\\r", "\r").replaceAll("\\n\\n", "\n");
								str = Pattern.compile("^(.)", Pattern.MULTILINE).matcher(str).replaceAll("Sample\t$1");
								System.out.print(str);
							}
						} catch (Exception ex) {
							String contentType = "";
							try {
								StringWriter w = new StringWriter();
								Model m = (Model) prepareQuery(endpointUrl, rq, "CONSTRUCT");
								m.write(w, "TTL");
								String str = w.toString();
								predSample.put(trp, str);
							} catch (Exception eqeh) {
								System.out.println("#Error in getPredicates: [" + contentType + "]" + ex.getMessage() + "\t" + rq);
							}
						} finally { qm.close(); }
					}
				}
				if(try_count < maxTryCount)
					System.out.println("#Success after error.");
				try_count = 0;
			} catch (Exception e) {
				System.out.println("#Error in getPredicates ["+ try_count + "] :" + erm);
			} finally { rs.close(); }
		}
	}

	static void mapClassLabels(String c, String t){
		String q = "SELECT DISTINCT ?c (STR(?l) AS ?lb) (STR(?cm) AS ?com) {"
				+ "?c a <" + c + "> ."
				+ "{{ ?c <" + RDFS.label.getURI() + "> ?l} UNION { ?c <" + RDFS.comment.getURI() + "> ?cm}}}";
		for(int try_count = maxTryCount; try_count > 0; try_count--) {
			for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
			ResultSet rs = (ResultSet) prepareQuery(endpointUrl, q, "SELECT");
			if(rs == null) return;
			try {
				for ( ; rs.hasNext() ;){
					QuerySolution sln = rs.nextSolution();
					String className = sln.get("c").toString();
					String classLabel = sln.get("lb").toString();
					System.out.println("Map\t" + t + "\t" + className + "\t" + classLabel);
					classLabels.put(className, classLabel);
				}
				if(try_count < maxTryCount)
					System.out.println("#Success after error.");
				try_count = 0;
			} catch (Exception e){
				System.out.println("#Error in mapClassLabels [" + try_count + "]");
			} finally { rs.close(); }
		}
	}

	static void mapClassLabels(){
		String q = "SELECT DISTINCT ?c (STR(?l) AS ?lb) (STR(?com) AS ?cm) {"
				+ "?i a ?c . {{?c <" + RDFS.label.getURI() + "> ?l} UNION {?c <" + RDFS.comment.getURI() + "> ?com}}}";
		for(int try_count = maxTryCount; try_count > 0; try_count--) {
			for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
			ResultSet rs = (ResultSet)prepareQuery(endpointUrl, q, "SELECT");
			if(rs == null) return;
			try {
				for ( ; rs.hasNext() ;){
					QuerySolution sln = rs.nextSolution();
					String className = sln.get("c").toString();
					String classLabel = sln.get("lb").toString();
					String classComment = sln.get("cm").toString();
					System.out.println("Map\tClass\t" + className + "\t" + classLabel + "\t" + classComment);
					//classLabels.put(className, classLabel);
				}
				if(try_count < maxTryCount)
					System.out.println("#Success after error.");
				try_count = 0;
			} catch (Exception e){
				System.out.println("#Error in mapClassLabels [" + try_count + "]");
			} finally { rs.close(); }
		}
	}

	static void printStats(){
		System.out.println("<< Obtained Statistics >>");
		System.out.println("# Class #");
		for(RDFNode r: classData.keySet()){
			System.out.println(r + ": " + classData.get(r));
		}
		System.out.println("# Predicate #");
		for(RDFNode r: predData.keySet()){
			System.out.println(r + ": " + predData.get(r));
			System.out.println(r + ": " + predClassData.get(r).size());
			for(int idx = 0;idx < predDomain.get(r).size(); idx++){
				RDFNode dom = predDomain.get(r).get(idx);
				RDFNode ran = predRange.get(r).get(idx);
				if(dom.toString() == "[unknown]" && ran.toString() == "[unknown]")
					continue;
				System.out.println("domain/range/count: " + dom + " / " + ran + " / " + predRelCount.get(Arrays.asList(r,dom,ran)));
			}
		}
	}

	static Model makeRDFdata(String[] graphUriSet){
		String ns_sd = "http://www.w3.org/ns/sparql-service-description#";
		String ns_void = "http://rdfs.org/ns/void#";
		String ns_sbm = "http://sparqlbuilder.org/2015/09/rdf-metadata-schema#";
		Model m = ModelFactory.createDefaultModel();
		m.setNsPrefix("sd", ns_sd);
		m.setNsPrefix("void", ns_void);
		m.setNsPrefix("sbm", ns_sbm);

		Property void_property = m.createProperty(ns_void + "property");
		Property void_triples = m.createProperty(ns_void + "triples");
		Property void_classes = m.createProperty(ns_void + "classes");
		Property void_class = m.createProperty(ns_void + "class");
		Property void_properties = m.createProperty(ns_void + "properties");
		Property void_entities = m.createProperty(ns_void + "entities");
		Property void_propertyPartition = m.createProperty(ns_void + "propertyPartition");
		Property void_classPartition = m.createProperty(ns_void + "classPartition");

		Property sbm_classRelation = m.createProperty(ns_sbm + "classRelation");
		Property sbm_subjectClass = m.createProperty(ns_sbm + "subjectClass");
		Property sbm_objectClass = m.createProperty(ns_sbm + "objectClass");
		Property sbm_sample = m.createProperty(ns_sbm + "sample");

		Resource root = m.createResource();
		Resource dataset = m.createResource();
		dataset.addProperty(RDF.type, m.createResource(ns_sd + "Dataset"));
		dataset.addProperty(void_classes, m.createTypedLiteral((long)Classes.size()));
		dataset.addProperty(void_properties, m.createTypedLiteral((long)Properties.size()));
		dataset.addProperty(void_triples, m.createTypedLiteral(totalTriples));

		root.addProperty(RDF.type, m.createResource(ns_sd + "Service"));
		root.addProperty(m.createProperty(ns_sd + "endpoint"), m.createResource(customEpUrl));
		root.addProperty(m.createProperty(ns_sd + "defaultDataset"), dataset);

		Resource void_Dataset = m.createResource(ns_void + "Dataset");
		Resource sbm_ClassRelation = m.createResource(ns_sbm + "ClassRelation");

		for(RDFNode r: predData.keySet()){
			Resource propPart = m.createResource();
			propPart.addProperty(RDF.type, void_Dataset);
			propPart.addProperty(void_property, r);
			propPart.addProperty(void_triples, m.createTypedLiteral(predData.get(r)));
			propPart.addProperty(void_classes, m.createTypedLiteral((long)predClassData.get(r).size()));

			for(int idx = 0;idx < predDomain.get(r).size(); idx++){
				RDFNode dom = predDomain.get(r).get(idx);
				RDFNode ran = predRange.get(r).get(idx);
				if(dom.toString() == "[unknown]" && ran.toString() == "[unknown]")
					continue;
				Resource cr = m.createResource();
				cr.addProperty(RDF.type, sbm_ClassRelation);
				if(dom.toString() != "[unknown]")
					cr.addProperty(sbm_subjectClass, dom);
				if(ran.toString() != "[unknown]")
					cr.addProperty(sbm_objectClass, ran);
				List<RDFNode> rdr = Arrays.asList(r,dom,ran);
				cr.addProperty(void_triples, m.createTypedLiteral(predRelCount.get(rdr)));
				String smpl = predSample.containsKey(rdr)? predSample.get(rdr):null;
				if(smpl != null)
					cr.addProperty(sbm_sample, m.createTypedLiteral(smpl));
				propPart.addProperty(sbm_classRelation, cr);
			}

			dataset.addProperty(void_propertyPartition, propPart);
		}

		for(RDFNode r: classData.keySet()){
			Resource classPart = m.createResource();
			classPart.addProperty(RDF.type, void_Dataset);
			classPart.addProperty(void_class, r);
			classPart.addProperty(void_entities, m.createTypedLiteral(classData.get(r)));
			dataset.addProperty(void_classPartition, classPart);
		}

		Resource ngr = m.createResource()
				.addProperty(RDF.type, m.createResource(ns_sd + "NamedGraph"));
		for (String s: graphUriSet)
			ngr.addProperty(m.createProperty(ns_sd + "name"), m.createResource(s));
		dataset.addProperty(m.createProperty(ns_sd + "namedGraph"), ngr); 

		FastDateFormat fdfIso8601ExtendedFormat = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ssZZ");
		Resource clog = m.createResource();
		clog.addProperty(RDF.type, m.createResource(ns_sbm + "CrawlLog"));
		clog.addProperty(m.createProperty(ns_sbm + "crawlStartTime"), m.createTypedLiteral(fdfIso8601ExtendedFormat.format(start), XSDDatatype.XSDdateTime));
		clog.addProperty(m.createProperty(ns_sbm + "crawlEndTime"), m.createTypedLiteral(fdfIso8601ExtendedFormat.format(end), XSDDatatype.XSDdateTime));
		clog.addProperty(m.createProperty(ns_sbm + "endpointAccesses"), m.createTypedLiteral(nOfQueries));
		dataset.addProperty(m.createProperty(ns_sbm + "crawlLog"), clog);

		return m;
	}

	static void applyGraphs(String[] graphUriSet) throws HttpException {
		nOfQueries = 0;
		totalTriples = 0;
		Classes = new HashSet<RDFNode>();
		Properties = new HashSet<RDFNode>();
		classLabels = new HashMap<String,String>();
		predData = new HashMap<RDFNode,Long>();
		classData = new HashMap<RDFNode,Long>();
		predClassData = new HashMap<RDFNode,HashSet<RDFNode>>();
		predDomain = new HashMap<RDFNode,List<RDFNode>>();
		predRange = new HashMap<RDFNode,List<RDFNode>>();
		predRelCount = new HashMap<List<RDFNode>,Long>();
		predSample = new HashMap<List<RDFNode>,String>();

		start = Calendar.getInstance().getTime();

		String fromList = "";
		for (String s: graphUriSet) {
			fromList += "FROM <" + s + "> ";
		}

		// Obtains a number of triples in the graph
		String tf = "SELECT (COUNT(*) AS ?t)"
				+ (graphAcceptable? fromList:"")
				+ "WHERE {?s ?p ?o .}";
		for(int try_count = maxTryCount; try_count > 0; try_count--) {
			for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
			ResultSet rs = (ResultSet) prepareQuery(endpointUrl, tf, "SELECT");
			if(rs == null) return;
			try {
				totalTriples = rs.next().get("t").asLiteral().getLong();
				if(!(sbmFlag || silentFlag))
					System.out.println("Total\t" + totalTriples);
				if(try_count < maxTryCount)
					System.out.println("#Success after error.");
				try_count = 0;
			} catch (Exception e){
				System.out.println("#Error in applyGraphs [" + try_count + "]:" + e.getMessage());
			} finally { rs.close(); }
		}

		// Obtains a class list
		String cqs = "SELECT DISTINCT ?c "
				+ (graphAcceptable? fromList:"")
				+ "WHERE {[] a ?c .}";
		//System.out.println(cqs);
		for(int try_count = maxTryCount; try_count > 0; try_count--) {
			for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
			ResultSet rs = (ResultSet) prepareQuery(endpointUrl, cqs, "SELECT");
			if(rs == null) return;
			try {
				for ( ; rs.hasNext() ;){
					QuerySolution sln = rs.nextSolution();
					RDFNode cls = sln.get("c");
					String className = cls.toString();
					String classLabel = classLabels.containsKey(className)? "\t" + classLabels.get(className):""; 
					if(!(sbmFlag || silentFlag))
						System.out.println("Class\t" + className + classLabel);
					Classes.add(cls);
				}
				if(try_count < maxTryCount)
					System.out.println("#Success after error.");
				try_count = 0;
			} catch (Exception e){
				System.out.println("#Error in applyGraphs [" + try_count +"]:" + e.getMessage());
			} finally { rs.close(); }
		}

		if(Classes.size() == 0){
			if(!(sbmFlag || silentFlag))
				System.out.println("No classes.");
			return;
		}

		int isInstancePresent;
		// for each combination of all the classes
		for (RDFNode c1: Classes){
			// counts instances which belong to the class c1
			String c1name = c1.toString();
			long c1count = 0;
			String c1lb = classLabels.containsKey(c1name)? classLabels.get(c1name):c1name;
			String cquery = "SELECT (COUNT(?s) AS ?rc) "
					+ (graphAcceptable? fromList:"")
					+ "WHERE {?s a <" + c1 + "> .}";
			//System.out.println(cquery);
			for(int try_count = maxTryCount; try_count > 0; try_count--) {
				for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
				ResultSet cs = (ResultSet) prepareQuery(endpointUrl, cquery, "SELECT");
				if(cs == null)	return;
				try {
					c1count = cs.next().get("rc").asLiteral().getLong();
					if(!(sbmFlag || silentFlag))
						System.out.println("CCnt\t" + c1count + "\t" + c1lb);
					if(classData.containsKey(c1))
						classData.put(c1, classData.get(c1) + c1count);
					else
						classData.put(c1, c1count);
					if(try_count < maxTryCount)
						System.out.println("#Success after error.");
					try_count = 0;
				} catch (Exception e) {
					System.out.println("#Error Occurred in applyGraphs [" + try_count + "]: " + cquery);
				} finally { cs.close(); }
			}
			if(c1count == 0)
				continue;

			// find any triples that connects a given class as the subject, obtain the predicates, and the object classes 
			String yacquery = "SELECT ?p ?c2 (COUNT(?p) AS ?pc) "
					+ (graphAcceptable? fromList:"")
					+ "WHERE { <" + c1 + "> ^<" + RDF.type.getURI() + "> [ ?p [ <" + RDF.type.getURI() + "> ?c2 ]] ."
					+ "FILTER (?p != <" + RDF.type.getURI() + ">)"
					+ "} GROUP BY ?p ?c2";
			//System.out.println(yacquery);
			for(int try_count = maxTryCount; try_count > 0; try_count--) {
				for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
				ResultSet cs = (ResultSet) prepareQuery(endpointUrl, yacquery, "SELECT");
				if(cs == null) return;
				try {
					for ( ; cs.hasNext() ;){
						QuerySolution rsn = cs.nextSolution();
						RDFNode c2 = rsn.get("c2");
						if(c2.toString().equals(OWL.Class.toString()))
							continue;
						long x = rsn.get("pc").asLiteral().getLong();
						RDFNode prd = rsn.get("p");
						String pred = prd.toString();
						if(!(sbmFlag || silentFlag))
							System.out.println("Crel\t" + x + "\t" + c1name + " -> " + c2.toString());
						Properties.add(prd);
						if(predData.containsKey(prd)){
							predData.put(prd, predData.get(prd) + x);
						}else{
							predData.put(prd, x);
							predClassData.put(prd, new HashSet<RDFNode>());
							predDomain.put(prd, new ArrayList<RDFNode>());
							predRange.put(prd, new ArrayList<RDFNode>());
						}
						predClassData.get(prd).add(c1);
						predClassData.get(prd).add(c2);
						RDFNode dom = c1;
						RDFNode ran = c2;
						predDomain.get(prd).add(dom);
						predRange.get(prd).add(ran);
						List<RDFNode> trp = Arrays.asList(prd,dom,ran);
						if(predRelCount.containsKey(trp))
							predRelCount.put(trp, predRelCount.get(trp) + x);
						else
							predRelCount.put(trp, x);

						if(enableConstruct){
							String _rsquery = "CONSTRUCT {?s <" + pred + "> ?o} "
									+ (graphAcceptable? fromList:"")
									+ "WHERE {?s <" + pred + "> ?o . ?s a <" + c1 + "> . ?o a <" + c2 + "> .}"
									+ " LIMIT 3";
							if(!(sbmFlag || silentFlag)){
								String predLabel = classLabels.containsKey(pred)? "\t" + classLabels.get(pred):"";
								System.out.println("Relp\t" + x + "\t" + pred + predLabel);
							}
							boolean failure = true;
							for(int inner_try_count = 3; inner_try_count > 0 && failure; inner_try_count--) {
								//System.out.println(_rsquery);
								Model qm = (Model) prepareQuery(endpointUrl, _rsquery, "CONSTRUCT");
								if(qm == null) return;
								waitAbit();
								try {
									StringWriter w = new StringWriter();
									qm.write(w, "TTL");
									String str = w.toString();
									predSample.put(trp, str);
									if(!(sbmFlag || silentFlag)){
										str = str.replaceAll("\\r\\n\\r\\n", "\r\n").replaceAll("\\r\\r", "\r").replaceAll("\\n\\n", "\n");
										str = Pattern.compile("^(.)", Pattern.MULTILINE).matcher(str).replaceAll("Sample\t$1");
										System.out.print(str);
									}
									failure = false;
								} catch (Exception ex) {
									System.out.println("#Error in applyGraphs (sampling) [" + inner_try_count + "]\t" + ex.getMessage() + "\t" + _rsquery);
								} finally { qm.close(); }
							}
						}
					}
					if(try_count < maxTryCount)
						System.out.println("#Success after error.");
					try_count = 0;
				} catch (Exception e) {
					System.out.println("#Error Occurred in applyGraphs [" + try_count + "]: " + yacquery);
				} finally { cs.close(); }
			}

			if (countUnknownClass) {
				// counts triples whose subject and object belong to c1 and an unknown class, respectively
				String curquery = "SELECT (COUNT(?s) AS ?rc) "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {<" + c1 + "> ^<" + RDF.type.getURI() + "> / !<" + RDF.type.getURI() + "> ?o . "
						+ "FILTER(!isLiteral(?o) && NOT EXISTS{?o <" + RDF.type.getURI() + "> ?oc})}";
				String alt_curquery = "SELECT (?oc1 - ?oc2 AS ?rc) "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {"
						+ "{SELECT (COUNT(?o) AS ?oc1) { ?s ?p ?o . ?s a <" + c1 + "> . "
						+ "FILTER(!isLiteral(?o) && ?p != <" + RDF.type.getURI() + ">) }}"
						+ "{SELECT (COUNT(?o) AS ?oc2) {{SELECT DISTINCT ?s ?p ?o { ?s ?p ?o . ?s a <" + c1 + "> . "
						+ "?o a ?oc . FILTER(?p != <" + RDF.type.getURI() + ">) }}}}"
						+ "}";
				isInstancePresent = getClassRelations(curquery, c1lb, "(Unknown Class Resource)",
						"This endpoint doesn't seem to suuport the property path. (c1 -> Unknown) " + curquery);
				if(isInstancePresent < 0){
					getClassRelations(alt_curquery, c1lb, "(Unknown Class Resource)",
							"This endpoint doesn't seem work properly. Maybe timeout. (c1 -> Unknown) " + alt_curquery);
				}
				if(isInstancePresent > 0){
					// enumerates all the predicates and shows some triples for each predicate
					String rpquery = "SELECT ?p (COUNT(?p) AS ?rc) "
							+ (graphAcceptable? fromList:"")
							+ "WHERE {?s ?p ?o . ?s a <" + c1 + "> . "
							+ "FILTER(!isLiteral(?o) && NOT EXISTS{?s a ?o} && NOT EXISTS{?o a ?oc})}"
							+ " GROUP BY ?p";
					String _rspquery = "CONSTRUCT {?s <__PRED__> ?o} "
							+ (graphAcceptable? fromList:"")
							+ "WHERE {?s <__PRED__> ?o ; a <" + c1 + "> . "
							+ "FILTER(!isLiteral(?o) && NOT EXISTS {?o a ?oc})}"
							+ " LIMIT 3";
					getPredicates(rpquery, _rspquery,
							"This endpoint doesn't seem to suuport NOT EXISTS.",
							c1, rsc);
				}

				// counts triples whose subject and object belong to an unknown class and c1, respectively
				String ucrquery = "SELECT (COUNT(?s) AS ?rc) "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {?s !<" + RDF.type.getURI() + "> [ a <" + c1 + "> ] . "
						+ "FILTER NOT EXISTS {?s a ?sc}}";
				String alt_ucrquery = "SELECT (?oc1 - ?oc2 AS ?rc) "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {{SELECT (COUNT(?s) AS ?oc1) {?o a <" + c1 + "> . ?s ?p ?o. }}"
						+ "{SELECT (COUNT(?s) AS ?oc2) {{SELECT DISTINCT ?s ?p ?o {?o a <" + c1 + "> . ?s a ?sc . ?s ?p ?o.}}}}"
						+ "}";
				isInstancePresent = getClassRelations(ucrquery, "(Unknown Class Resource)", c1lb,
						"This endpoint doesn't seem to suuport the property path. (Unknown -> c1) " + ucrquery);
				if(isInstancePresent < 0){
					getClassRelations(alt_ucrquery, "(Unknown Class Resource)", c1lb,
							"This endpoint doesn't seem work properly. Maybe timeout. (Unknown -> c1) " + alt_ucrquery);				
				}
				if(isInstancePresent > 0){
					// enumerates all the predicates and shows some triples for each predicate
					String urpquery = "SELECT ?p (COUNT(?p) AS ?rc) "
							+ (graphAcceptable? fromList:"")
							+ "WHERE {?s ?p [ a <" + c1 + "> ]. "
							+ "FILTER NOT EXISTS {?s a ?oc} }"
							+ " GROUP BY ?p";
					String _urpsquery = "CONSTRUCT {?s <__PRED__> ?o} "
							+ (graphAcceptable? fromList:"")
							+ "WHERE {?s <__PRED__> ?o . ?o a <" + c1 + "> . "
							+ "FILTER NOT EXISTS {?s a ?oc} }"
							+ " LIMIT 3";
					getPredicates(urpquery, _urpsquery,
							"This endpoint doesn't seem to suuport the NOT EXISTS keyword.",
							rsc, c1);
				}
			}

			// counts triples whose subject belongs to c1 and whose object is Literal
			String clrquery = "SELECT (COUNT(?s) AS ?rc) "
					+ (graphAcceptable? fromList:"")
					+ "WHERE {?s !<" + RDF.type.getURI() + "> ?o ; a <" + c1 + "> . "
					+ "FILTER(isLiteral(?o))}"; // Object is literal, so its predicate shouldn't rdf:type, but just for performance
			isInstancePresent = getClassRelations(clrquery, c1lb, "(Literal)",
					"This endpoint doesn't seem to support the isLiteral function.");
			if(isInstancePresent > 0){
				// enumerates all the predicates and shows some triples for each predicate
				String ulpquery = "SELECT ?p (COUNT(?p) AS ?rc) "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {?s ?p ?o ; a <" + c1 + "> . "
						+ "FILTER(isLiteral(?o))}"
						+ " GROUP BY ?p";
				String _ulpsquery = "CONSTRUCT {?s <__PRED__> ?o} "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {?s <__PRED__> ?o ; a <" + c1 + "> . "
						+ "FILTER(isLiteral(?o))}"
						+ " LIMIT 3";
				getPredicates(ulpquery, _ulpsquery,
						"This endpoint doesn't seem to support the isLiteral function.",
						c1, lit);
			}
		} // end of the outer for loop ( String c1: Classes )

		if (countUnknownClass) {
			// counts triples whose subject and object belong to any unknown class, respectively
			String uurquery = "SELECT (COUNT(?s) AS ?rc) "
					+ (graphAcceptable? fromList:"")
					+ "WHERE {?s ?p ?o . "
					+ "FILTER(!isLiteral(?o) && NOT EXISTS {?s a ?sc} && NOT EXISTS {?o a ?oc})}";
			String alt_uurquery = "SELECT (?sc1 - (?sc2 + ?sc3 - ?sc4) AS ?rc) "
					+ (graphAcceptable? fromList:"")
					+ "WHERE {"
					+ "{ SELECT (count(*) AS ?sc1) {?s ?p ?o . FILTER(!isLiteral(?o)) }} "
					+ "{ SELECT (count(*) AS ?sc2) { { SELECT DISTINCT ?s ?p ?o {?s ?p ?o . ?o a ?oc . }}}} "
					+ "{ SELECT (count(*) AS ?sc3) { { SELECT DISTINCT ?s ?p ?o {?s ?p ?o . ?s a ?sc . FILTER(!isLiteral(?o)) }}}} "
					+ "{ SELECT (count(*) AS ?sc4) { { SELECT DISTINCT ?s ?p ?o {?s ?p ?o . ?s a ?sc . ?o a ?oc }}}} "
					+ "}";
			isInstancePresent = getClassRelations(uurquery, "(Unknown Class Resource)", "(Unknown Class Resource)",
					"This endpoint doesn't seem to suuport the NOT EXISTS keyword. (Unknown -> Unknown)" + uurquery);
			if(isInstancePresent < 0){
				getClassRelations(alt_uurquery, "(Unknown Class Resource)", "(Unknown Class Resource)",
						"This endpoint doesn't seem work properly. Maybe timeout. (Unknown -> Unknown) " + alt_uurquery);				
			}
			if(isInstancePresent > 0){
				// enumerates all the predicates and shows some triples for each predicate
				String uupquery = "SELECT ?p (COUNT(?p) AS ?rc) "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {?s ?p ?o . "
						+ "FILTER(!isLiteral(?o) && NOT EXISTS {?s a ?sc} && NOT EXISTS {?o a ?oc})}"
						+ " GROUP BY ?p";
				String _uupsquery = "CONSTRUCT {?s <__PRED__> ?o} "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {?s <__PRED__> ?o . "
						+ "FILTER(!isLiteral(?o) && NOT EXISTS {?s a ?sc} && NOT EXISTS {?o a ?oc})}"
						+ " LIMIT 3";
				getPredicates(uupquery, _uupsquery,
						"This endpoint doesn't seem to suuport the NOT EXISTS keyword.",
						rsc, rsc);
			}

			// counts triples whose subject belongs to an unknown class and whose object is literal
			String ulrquery = "SELECT (COUNT(?s) AS ?rc) "
					+ (graphAcceptable? fromList:"")
					+ "WHERE {?s !<" + RDF.type.getURI() + "> ?o . "
					+ "FILTER(isLiteral(?o) && NOT EXISTS {?s a ?sc})}";
			String alt_ulrquery = "SELECT (?sc1 - ?sc2 AS ?rc) "
					+ (graphAcceptable? fromList:"")
					+ "WHERE { SELECT (count(*) AS ?sc1) {?s ?p ?o . FILTER(isLiteral(?o)) }}"
					+ "{ SELECT (count(*) AS ?sc2) { { SELECT DISTINCT ?s ?p ?o {?s ?p ?o . ?s a ?sc . FILTER(isLiteral(?o)) }}}}"
					+ "}";
			isInstancePresent = getClassRelations(
					ulrquery, "(Unknown Class Resource)", "(Literal)",
					"This endpoint doesn't seem to suuport the property paths and/or the isLiteral function." + ulrquery);
			if(isInstancePresent < 0){
				getClassRelations(alt_ulrquery, "(Unknown Class Resource)", "(Literal)",
						"This endpoint doesn't seem work properly. Maybe timeout. (Unknown -> Literal) " + alt_ulrquery);				
			}
			if(isInstancePresent > 0){
				// enumerates all the predicates and shows some triples for each predicate
				String ulpquery = "SELECT ?p (COUNT(?p) AS ?rc) "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {?s ?p ?o . "
						+ "FILTER(isLiteral(?o) && NOT EXISTS {?s a ?sc})}"
						+ " GROUP BY ?p";
				String _ulpsquery = "CONSTRUCT {?s <__PRED__> ?o} "
						+ (graphAcceptable? fromList:"")
						+ "WHERE {?s <__PRED__> ?o . "
						+ "FILTER(isLiteral(?o) && NOT EXISTS {?s a ?sc})}"
						+ " LIMIT 3";
				getPredicates(ulpquery, _ulpsquery,
						"This endpoint doesn't seem to suuport the MINUS keyword and/or the isLiteral function.",
						rsc, lit);
			}
		}
		end = Calendar.getInstance().getTime();
		// printStats();
		String[] ngs = {"file:///TripleDataProfiler"};
		if( graphUriSet.length > 0 )
			ngs = graphUriSet;
		Model m = makeRDFdata(ngs);
		if(sbmFlag && !silentFlag)
			m.write(System.out, "TTL");
		if(insertFlag)
			insertData(m);
	}

	static void applyGraph(String graphUri) throws HttpException {
		final String[] _arg = {graphUri}; 
		applyGraphs( _arg );
	}

	static boolean getGraphsAndApply(String query){
		for(int try_count = maxTryCount; try_count > 0; try_count--) {
			for(int _wait = maxTryCount; _wait >= try_count; _wait--) waitAbit();
			ResultSet rs  = (ResultSet) prepareQuery(endpointUrl, query, "SELECT");
			if(rs == null) return false;
			try {
				graphIteration:
				for ( ; rs.hasNext() ;){
					graphAcceptable = true;
					QuerySolution sln = rs.nextSolution();
					String graphName = sln.get("g").toString();
					if(avoidableGraph.contains(graphName))
						continue;
					for(String p: avoidablePatterns)
						if(graphName.matches(p))
							continue graphIteration;
					if(!(sbmFlag || silentFlag))
						System.out.println("Graph\t" + graphName);
					try {
						applyGraph(graphName);
					} catch (HttpException e) {
						System.out.println(e.getMessage());
					}
				}
				if(!graphAcceptable)
					try {
						applyGraph(null);
					} catch (HttpException e){
						System.out.println("Http Exception!");
						System.out.println(e.getMessage());
					}
				if(try_count < maxTryCount)
					System.out.println("#Success after error.");
				try_count = 0;
			} catch ( Exception e) {
				System.out.println("#Error in getGraphsAndApply [" + try_count + "]: " + e.getMessage());
				return false;
			} finally { rs.close(); }
		}
		return true;
	}

	static String[] avoidablePatterns =
		{
		"http://localhost:\\d+/sparql",
		"http://localhost:\\d+/DAV/?",
		"http://\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+/sparql",
		"http://\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+/DAV/?"
		}; 
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		JenaSystem.init();
		Options options = new Options();
		options.addOption("ep", true, "Endpoint URL");
		options.addOption("k", true, "api key");
		options.addOption("proxy_host", true, "host name for proxy server");
		options.addOption("proxy_port", true, "port number for proxy server");
		options.addOption("custom_endpoint", true, "endpoint URL for sd:endpoint object");
		options.addOption("w", true, "wait time");
		options.addOption("timeout", true,"timeout period");
		options.addOption("sbm", false, "sbm-compatible RDF output");
		options.addOption("s", false, "silent (no output to stdout except exceptions)");
		options.addOption("i", false, "insert obtained RDF data to a repository using SPARQL Update");
		options.addOption("u", false, "count instances not belong to any explicitly asserted class by rdf:type");
		options.addOption("iep", true, "SPARQL endpoint to which obtained RDF data are inserted");
		options.addOption("get", false, "use HTTP GET method to access the SPARQL endpoint");
		options.addOption("construct", false, "Use construct to get examples");
		Option avoidGraphArgs = new Option("agn", true, "graph name(s) to be ignored");
		avoidGraphArgs.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(avoidGraphArgs);
		Option graphArgs = new Option("gn", true, "graph name(s) to be retrieved");
		graphArgs.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(graphArgs);

		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse(options, args);
			endpointUrl = cmd.getOptionValue("ep");
			apikey = cmd.getOptionValue("k");
			proxyHost = cmd.getOptionValue("proxy_host");
			proxyPort = cmd.getOptionValue("proxy_port");
			enableConstruct = cmd.hasOption("construct");
			String wt = cmd.getOptionValue("w");
			String timeout = cmd.getOptionValue("timeout");
			sbmFlag = cmd.hasOption("sbm");
			insertFlag = cmd.hasOption("i");
			silentFlag = cmd.hasOption("s");
			getFlag = cmd.hasOption("get");
			countUnknownClass = cmd.hasOption("u");
			if(wt != null)
				waittime = Integer.parseInt(wt);
			if(timeout != null)
				shortTimeOut = Integer.parseInt(timeout);
			args = cmd.getArgs();
			String iep = cmd.getOptionValue("iep");
			if(iep != null)
				repoEndpointUrl = iep;
			givenGraphs = cmd.getOptionValues("gn");
			givenAvoidGraphs = cmd.getOptionValues("agn");
			if( givenAvoidGraphs != null)
				for( String gag: givenAvoidGraphs )
					avoidableGraph.add(gag);
			customEpUrl = cmd.getOptionValue("custom_endpoint");
			if( customEpUrl == null)
				customEpUrl = endpointUrl;
		} catch(ParseException exp){
			System.err.println( exp.getMessage() );
			System.out.println(shortHelpMessage);
			return;
		}

		if(endpointUrl == null){
			Date d = new Date();
			System.out.println(shortHelpMessage + "\n-----");
			System.out.println(d);
			System.exit(0);
		}

		if (proxyHost != null) {
			System.setProperty("ProxyHost", proxyHost);
			if (proxyPort != null) System.setProperty("ProxyPort", proxyPort);
			System.out.println(proxyHost + ":" + proxyPort);
		}
		if(givenGraphs != null){
			graphAcceptable = true;
			try {
				applyGraphs(givenGraphs);
				System.exit(0);
			} catch (HttpException e){
				System.out.println(e.getMessage());
			}
		} else {
			if(!(getGraphsAndApply(queryGraphs) || getGraphsAndApply(altQueryGraphs))) {
				try {
					applyGraph(null);
					System.exit(0);
				} catch (HttpException e){
					System.out.println("Http Exception!");
					System.out.println(e.getMessage());
				}
			}
		}
	}

}