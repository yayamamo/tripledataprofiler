#!/usr/bin/env python3

import sys
import rdflib
from rdflib.namespace import OWL, RDF, RDFS, VOID, XMLNS, XSD
from rdflib import Namespace

g = rdflib.Graph()
if len(sys.argv) != 2:
    print("Needs a turtle file name.")
    sys.exit()

# print(sys.argv[1])
g.parse(sys.argv[1], format="turtle")
g.bind("void", VOID)

# print(len(g))
print("""digraph {
  ranksep=2;
  rankdir="LR";
  charset="utf-8";
  layout="circo";""")

qres = g.query(
    """SELECT ?c ?e
       WHERE {
        [] void:classPartition [ void:class ?c; void:entities ?e ]
       }""")

print("// The number of entities")
for row in qres:
    print('  "<' + str(row[0]) + '>" [ label="<' + str(row[0]) + '>\\n' + row[1] +'" ];')

qres = g.query(
    """SELECT ?s ?o ?p ?e
       WHERE {
        [] void:propertyPartition [ void:property ?p; void:triples ?e ; sbm:classRelation [ sbm:subjectClass ?s ; sbm:objectClass ?o ] ]
       }""")

print("// Class relations with the numbers of edges")
count = 0
for row in qres:
    if row[1] == rdflib.term.URIRef('http://www.w3.org/2000/01/rdf-schema#Literal'):
        print('  "<' + str(row[0]) + '>" -> "Literal (' + str(count) + ')" [ label="<' + str(row[2]) + '>\\n' + row[3] +'" ];')
        print('  "Literal (' + str(count) + ')" [ shape = box ];')
        count = count + 1
    else:
        print('  "<%s>" -> "<%s>" [ label="<%s>\\n %s" ];' % row)
print("}")